<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Schedule to San Antonio's Artsy Texas! account
        $schedule->command('bot:sync-leads 153038')->everyFiveMinutes();

        // Schedule to Austin's Artsy Texas! account
        $schedule->command('bot:sync-leads 153033')->everyFiveMinutes();

        // Schedule to Dallas Artsy Events account
        $schedule->command('bot:sync-leads 150499')->everyFiveMinutes();

        // Schedule to Houston Artsy Events account
        $schedule->command('bot:sync-leads 148540')->everyFiveMinutes();

        // Schedule to Phoenix & Tucson's Artsy Events account
        $schedule->command('bot:sync-leads 126256')->everyFiveMinutes();

        // Schedule to Denver & Co Springs' Artsy Events! account
        $schedule->command('bot:sync-leads 63468')->everyFiveMinutes();

        // Schedule to Salt Lake City's Best Party Artists account
        $schedule->command('bot:sync-leads 155471')->everyFiveMinutes();

        // Schedule to San Diego's Best Party Artists account
        $schedule->command('bot:sync-leads 155516')->everyFiveMinutes();

        // Schedule to Seattle's Best Party Artists account
        $schedule->command('bot:sync-leads 155453')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
