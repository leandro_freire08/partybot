<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Leads;
use App\Models\Rules;
use App\Repositories\AccountRepository;
use App\Repositories\LeadsRepository;
use App\Repositories\RulesRepository;
use App\Services\TheBashService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use timgws\JoinSupportingQueryBuilderParser;
use timgws\QueryBuilderParser;

class TestQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:test-lead-answer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param TheBashService $theBashService
     * @param AccountRepository $accountRepository
     * @param RulesRepository $rulesRepository
     * @throws \timgws\QBParseException
     */
    public function handle(
        RulesRepository $rulesRepository,
        LeadsRepository $leadsRepository
    ) {

        /** @var Leads $lead */
        $lead = $leadsRepository->all()->first();

        /** @var Rules $rules */
        $rules = $rulesRepository->all(['account_id' => $lead->account_id])->sortBy('sort_order');

        foreach ($rules as $rule) {
            $qbp = new QueryBuilderParser();
            $table = DB::table('leads');
            $query = $qbp->parse($rule->filter, $table);

            $daysToEventCondition = [];
            foreach ($query->wheres as $key => $where) {
                if ($where['column'] == 'days_to_event') {
                    $daysToEventCondition = $where;
                    unset($query->wheres[$key]);
                    $query->wheres = array_values($query->wheres);
                }
            }

            if (!empty($daysToEventCondition)) {

                $daysToEventCondition['column'] = 'event_date';

                $now = new Carbon('now', 'UTC');

                if ($daysToEventCondition['operator'] == "<=") {
                    $now->modify('-' . $daysToEventCondition['value'] . ' days');
                } else {
                    $now->modify('+' . $daysToEventCondition['value'] . ' days');
                }

                $daysToEventCondition['value'] = $now->toDateString();

                array_push($query->wheres, $daysToEventCondition);
            }

            $leadsToAnswer = $query->get();

            foreach ($leadsToAnswer as $lead) {
                $test = 0;
            }
        }
    }
}
