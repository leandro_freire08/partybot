<?php

namespace App\Console\Commands;

use App\Services\GmailService;
use Illuminate\Console\Command;

class SyncEmails extends Command
{
    /**
     * @var GmailService
     */
    private $googleService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:sync-emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Emails from Gmail';

    /**
     * Create a new command instance.
     * @param GmailService
     * @return void
     */
    public function __construct(GmailService $service)
    {
        parent::__construct();
        $this->googleService = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->googleService->getMessagesByFrom("noreply@github.com");
    }
}
