<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Repositories\AccountRepository;
use App\Services\TheBashService;
use Illuminate\Console\Command;

class SyncProfiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:sync-profiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync profiles provided  a given account';

    /**
     * @var TheBashService
     */
    protected $theBashService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        TheBashService $theBashService
    ) {
        parent::__construct();
        $this->theBashService = $theBashService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->theBashService->syncProfiles();
    }
}
