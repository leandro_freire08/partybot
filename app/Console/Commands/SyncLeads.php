<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Repositories\AccountRepository;
use App\Services\TheBashService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SyncLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:sync-leads {account}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Crawler to navigate through the site';

    /**
     * @var TheBashService
     */
    protected $theBashService;

    /**
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        TheBashService $theBashService,
        AccountRepository $accountRepository
    ) {
        parent::__construct();
        $this->theBashService = $theBashService;
        $this->accountRepository = $accountRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var Account $account */
        $accountId = $this->argument('account');

        $account = $this->accountRepository->all(['internal_account_id' => $accountId])->first();

        Log::info('Syncing account: ID: ' . $accountId . " |  Name: " . $account->account_name);

        $this->theBashService->syncLeads($account);
    }
}
