<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Repositories\AccountRepository;
use App\Services\TheBashService;
use Illuminate\Console\Command;

class SyncServices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:sync-services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync services provided by a given account';

    /**
     * @var TheBashService
     */
    protected $theBashService;

    /**
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        TheBashService $theBashService,
        AccountRepository $accountRepository
    ) {
        parent::__construct();
        $this->theBashService = $theBashService;
        $this->accountRepository = $accountRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var Account $account */
        $account = $this->accountRepository->all()->first();
        $this->theBashService->syncServices($account);
    }
}
