<?php

namespace App\Repositories;

use App\Models\Rules;
use App\Repositories\BaseRepository;

/**
 * Class RulesRepository
 * @package App\Repositories
 * @version October 31, 2019, 10:53 pm UTC
*/

class RulesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'filter',
        'account_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rules::class;
    }
}
