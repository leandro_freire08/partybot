<?php

namespace App\Repositories;

use App\Models\Leads;
use App\Repositories\BaseRepository;

/**
 * Class LeadsRepository
 * @package App\Repositories
 * @version October 24, 2019, 10:58 am UTC
*/

class LeadsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'gig_id',
        'event_date',
        'event_location',
        'event_start_time',
        'event_length',
        'event_guests',
        'event_services',
        'event_budget',
        'client_name',
        'client_email',
        'client_phone',
        'venue',
        'is_responded',
        'account_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Leads::class;
    }
}
