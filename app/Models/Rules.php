<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rules
 * @package App\Models
 * @version October 31, 2019, 10:53 pm UTC
 *
 * @property string name
 * @property string filter
 * @property integer account_id
 * @property integer hourly_rate
 * @property integer travel_charge
 * @property string text_response
 * @property string text_response_no
 * @property string sort_order
 */
class Rules extends Model
{
    use SoftDeletes;

    public $table = 'rules';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'filter',
        'account_id',
        'hourly_rate',
        'travel_charge',
        'text_response',
        'text_response_no',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'hourly_rate' => 'integer',
        'travel_charge' => 'integer',
        'text_response' => 'string',
        'text_response_no' => 'string',
        'filter' => 'string',
        'account_id' => 'string',
        'name' => 'string',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    private static $filters = [

        [
            'id' => 'event_date',
            'label' => 'Event Date',
            'type' => 'date',
            'validation' => [
                'format' => 'YYYY/MM/DD'
            ],
            'plugin' => 'datepicker',
            'plugin_config' => [
                'format' => 'yyyy/mm/dd',
                'todayBtn' => 'linked',
                'todayHighlight' => true,
                'autoclose' => true
            ]
        ],
        [
            'id' => 'event_location',
            'label' => 'Event Location (miles)',
            'type' => 'integer',
            'description' => 'Event location is within a certain distance (in miles)'
        ],
        [
            'id' => 'event_start_time',
            'label' => 'Event Start Time',
            'type' => 'time'
        ],
        [
            'id' => 'event_length',
            'label' => 'Event Duration (minutes)',
            'type' => 'integer',
            'description' => 'Event duration in Minutes'
        ],
        [
            'id' => 'event_guests',
            'label' => 'Event Guests',
            'type' => 'integer'
        ],
        [
            'id' => 'event_budget',
            'label' => 'Event Budget',
            'type' => 'double',
            'validation' => [
                'min' => 0,
                'step' => 0.01
            ]
        ],
        [
            'id' => 'days_to_event',
            'label' => 'Days to Event (days)',
            'type' => 'integer',
            'validation' => [
                'min' => 0
            ],
            'operators' => ['less_or_equal', 'greater_or_equal'],
            'description' => 'How many days to the event from today'
        ]
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function provider()
    {
        return $this->belongsTo(\App\Models\Account::class, 'account_id', 'id');
    }

    public static function getFieldsToFilter()
    {
        array_push(self::$filters, Services::getFieldsToFilter());
        return json_encode(self::$filters);
    }
}
