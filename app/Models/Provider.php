<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Provider
 * @package App\Models
 * @version October 24, 2019, 6:09 pm UTC
 *
 * @property string name
 * @property string url
 * @property string username
 * @property string password
 */
class Provider extends Model
{
    use SoftDeletes;

    public $table = 'providers';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'url',
        'username',
        'password'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'url' => 'string',
        'username' => 'string',
        'password' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'url' => 'required',
        'username' => 'required',
        'password' => 'required'
    ];


    public static function getNameById($id)
    {
        return self::query()->find($id)->name;
    }


}
