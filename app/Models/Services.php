<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Services
 * @package App\Models
 * @version November 8, 2019, 7:16 pm UTC
 *
 * @property integer account_id
 * @property string internal_service_id
 * @property string name
 */
class Services extends Model
{
    use SoftDeletes;

    public $table = 'services';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'account_id',
        'internal_service_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'account_id' => 'integer',
        'internal_service_id' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'internal_service_id' => 'required',
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function provider()
    {
        return $this->belongsTo(\App\Models\Account::class, 'account_id', 'id');
    }

    public static function getFieldsToFilter()
    {
        $services = self::all();

        $serviceData = [];
        foreach ($services as $service) {
            $serviceData[$service->name] = $service->name;
        }

        return [
                'id' => 'event_services',
                'label' => 'Service',
                'type' => 'integer',
                'input' => 'select',
                'values' => $serviceData,
                'operators' => ['equal', 'contains', 'not_contains']
        ];
    }


}
