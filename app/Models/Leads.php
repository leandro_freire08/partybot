<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Leads
 * @package App\Models
 * @version October 24, 2019, 10:58 am UTC
 *
 * @property integer id
 * @property string gig_id
 * @property string bid_id
 * @property string event_date
 * @property string event_location
 * @property string event_start_time
 * @property string event_length
 * @property string event_guests
 * @property string event_services
 * @property string event_budget
 * @property string client_name
 * @property string client_email
 * @property string client_phone
 * @property string venue
 * @property boolean is_responded
 * @property string account_id
 */
class Leads extends Model
{
    use SoftDeletes;

    public $table = 'leads';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'bid_id',
        'gig_id',
        'event_date',
        'event_location',
        'event_start_time',
        'event_length',
        'event_guests',
        'event_services',
        'event_budget',
        'client_name',
        'client_email',
        'client_phone',
        'venue',
        'is_responded',
        'account_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bid_id' => 'string',
        'gig_id' => 'string',
        'event_date' => 'datetime',
        'event_location' => 'string',
        'event_start_time' => 'string',
        'event_length' => 'string',
        'event_guests' => 'string',
        'event_services' => 'string',
        'event_budget' => 'string',
        'client_name' => 'string',
        'client_email' => 'string',
        'client_phone' => 'string',
        'venue' => 'string',
        'is_responded' => 'boolean',
        'account_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bid_id' => 'required',
        'gig_id' => 'required',
        'is_responded' => 'required',
        'account_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function provider()
    {
        return $this->belongsTo(\App\Models\Account::class, 'account_id', 'id');
    }
}
