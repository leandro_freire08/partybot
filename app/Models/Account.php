<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Account
 * @package App\Models
 * @version October 24, 2019, 3:50 pm UTC
 *
 * @property \App\Models\Provider provider
 * @property integer id
 * @property string internal_account_id
 * @property string account_name
 * @property string reply_email
 * @property string account_link
 * @property integer provider_id
 */
class Account extends Model
{
    use SoftDeletes;

    public $table = 'accounts';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'id',
        'internal_account_id',
        'account_name',
        'account_link',
        'provider_id',
        'reply_email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'internal_account_id' => 'string',
        'account_name' => 'string',
        'account_link' => 'string',
        'reply_email' => 'string',
        'provider_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'internal_account_id' => 'required',
        'account_name' => 'required',
        'account_link' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function provider()
    {
        return $this->belongsTo(\App\Models\Provider::class, 'provider_id', 'id');
    }

    public static function getNameById($id)
    {
        return self::query()->find($id)->account_name;
    }
}
