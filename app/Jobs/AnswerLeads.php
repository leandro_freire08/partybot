<?php

namespace App\Jobs;

use App\Models\Account;
use App\Models\Leads;
use App\Models\Rules;
use App\Repositories\AccountRepository;
use App\Repositories\RulesRepository;
use App\Services\TheBashService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use timgws\QueryBuilderParser;

class AnswerLeads implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Leads
     */
    public $lead;

    /**
     * Create a new job instance.
     *
     * @param Leads $lead
     * @return void
     */
    public function __construct(
        Leads $lead
    ) {
        $this->lead = $lead;
    }

    /**
     * @param TheBashService $theBashService
     * @param AccountRepository $accountRepository
     * @param RulesRepository $rulesRepository
     * @throws \timgws\QBParseException
     */
    public function handle(
        TheBashService $theBashService,
        AccountRepository $accountRepository,
        RulesRepository $rulesRepository
    ) {
        /** @var Account $account */
        $account = $accountRepository->find($this->lead->account_id);

        /** @var Rules $rules */
        $rules = $rulesRepository->all(['account_id' => $this->lead->account_id])->sortBy('sort_order');

        $answerAsNo = false;
        foreach ($rules as $rule) {
            $qbp = new QueryBuilderParser();
            $table = DB::table('leads')->where(['account_id' => $this->lead->account_id]);
            $query = $qbp->parse($rule->filter, $table);

            /** @var Builder $query */
            $query = $this->handleDaysToEventCondition($query);

            $leadToAnswer = $query->get()->firstWhere('id', $this->lead->id);

            if ($leadToAnswer) {
                $answerAsNo = false;
                $theBashService->answerLead($account, $this->lead, $rule, true);
                break;
            }

            $answerAsNo = true;
        }

        if ($answerAsNo) {
            $rule = $rulesRepository->all(['account_id' => $this->lead->account_id])->sortBy('sort_order')->first();
            $theBashService->answerLead($account, $this->lead, $rule, false);
        }

//            $leadsToAnswer = $query->get();
//
//            foreach ($leadsToAnswer as $lead) {
//                if ($this->lead->gig_id == $lead->gig_id) {
//                    $theBashService->answerLead($account, $this->lead, $rule, true);
//                } else {
//                    $theBashService->answerLead($account, $this->lead, $rule, false);
//                }
//                $answered = true;
//            }
//
//            if ($answered) {
//                break;
//            }
//        }
    }

    /**
     * @param Builder $query
     * @return Builder
     * @throws \Exception
     */
    private function handleDaysToEventCondition(Builder $query)
    {
        $daysToEventCondition = [];
        foreach ($query->wheres as $key => $where) {
            if (array_key_exists('column', $where) && $where['column'] == 'days_to_event') {
                $daysToEventCondition = $where;
                unset($query->wheres[$key]);
                $query->wheres = array_values($query->wheres);
            }
        }

        if (!empty($daysToEventCondition)) {

            $daysToEventCondition['column'] = 'event_date';
            $now = new Carbon('now', 'UTC');

            if ($daysToEventCondition['operator'] == "<=") {
                $now->modify('-' . $daysToEventCondition['value'] . ' days');
            } else {
                $now->modify('+' . $daysToEventCondition['value'] . ' days');
            }

            $daysToEventCondition['value'] = $now->toDateString();
            array_push($query->wheres, $daysToEventCondition);
        }

        return $query;
    }
}
