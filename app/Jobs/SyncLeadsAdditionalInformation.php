<?php

namespace App\Jobs;

use App\Models\Account;
use App\Models\Leads;
use App\Repositories\AccountRepository;
use App\Services\DriverBrowser;
use App\Services\TheBashService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncLeadsAdditionalInformation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Leads
     */
    public $lead;

    /**
     * Create a new job instance.
     *
     * @param Leads $lead
     * @return void
     */
    public function __construct(
        Leads $lead
    ) {
        $this->lead = $lead;
    }

    /**
     * Execute job
     *
     * @param TheBashService $theBashService
     */
    public function handle(
        TheBashService $theBashService
    ) {
        $theBashService->getLeadAdditionalInformation($this->lead);
        DriverBrowser::kill();
    }
}
