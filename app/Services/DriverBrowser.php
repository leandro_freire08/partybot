<?php

namespace App\Services;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\WebDriverCurlException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\Log;

class DriverBrowser
{
    /**
     * @var RemoteWebDriver
     */
    private static $driver;

    public static function getInstance() {

//        $host = "http://127.0.0.1:9515";
        $host = "http://localhost:4444/wd/hub";

        if (self::$driver == null) {

            $chromeOptions = new ChromeOptions();
            $chromeOptions->addArguments([
                '--headless',
                '--disable-gpu',
                '--no-sandbox'
            ]);

            $capabilities = DesiredCapabilities::chrome();
            $capabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

            try {
                self::$driver = RemoteWebDriver::create($host, $capabilities, 5000);
            } catch (WebDriverCurlException $e) {
                Log::error("Chromedriver service is not running: " . $e->getMessage());
            }
        }
        return self::$driver;
    }

    public function getDriver()
    {

        $host = "http://localhost:4444/wd/hub";

        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments([
            '--headless',
            '--disable-gpu',
            '--no-sandbox'
        ]);

        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        $driver = RemoteWebDriver::create($host, $capabilities, 10000);

        return $driver;
    }

    public static function kill()
    {
        if (self::$driver != null) {
            self::$driver->quit();
        }
        self::$driver = null;
    }
}
