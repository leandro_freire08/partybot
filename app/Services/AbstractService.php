<?php

namespace App\Services;

use Facebook\WebDriver\Remote\RemoteWebDriver;

abstract class AbstractService
{
    /**
     * @param RemoteWebDriver $driver
     * @param string $framework
     * @throws \Exception
     */
    protected function waitForAjax($driver, $framework='jquery')
    {
        // javascript framework
        switch($framework){
            case 'jquery':
                $code = "return jQuery.active;"; break;
            case 'prototype':
                $code = "return Ajax.activeRequestCount;"; break;
            case 'dojo':
                $code = "return dojo.io.XMLHTTPTransport.inFlight.length;"; break;
            default:
                throw new \Exception('Not supported framework');
        }

        do {
            sleep(3);
        } while ($driver->executeScript($code));
    }
}
