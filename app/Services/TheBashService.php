<?php

namespace App\Services;

use App\Jobs\AnswerLeads;
use App\Jobs\SyncLeadsAdditionalInformation;
use App\Mail\LeadAnsweredMail;
use App\Models\Account;
use App\Models\Leads;
use App\Models\Provider;
use App\Models\Rules;
use App\Repositories\AccountRepository;
use App\Repositories\LeadsRepository;
use App\Repositories\ProviderRepository;
use App\Repositories\RulesRepository;
use App\Repositories\ServicesRepository;
use Carbon\Carbon;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\WebDriverCurlException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class TheBashService extends AbstractService
{
    const LOGIN_USERNAME_ELEMENT_ID = "userNameInput";
    const LOGIN_PASSWORD_ELEMENT_ID = "passwordInput";
    const VIEW_LEAD_HOURLY_RATE = "7";

    /**
     * @var LeadsRepository
     */
    protected $leadsRepository;

    /**
     * @var ProviderRepository
     */
    protected $providerRepository;

    /**
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * @var ServicesRepository
     */
    protected $servicesRepository;

    /**
     * @var RulesRepository
     */
    protected $rulesRepository;

    /** @var DriverBrowser */
    protected $driverBrowser;

    public function __construct(
        LeadsRepository $leadsRepository,
        ProviderRepository $providerRepository,
        AccountRepository $accountRepository,
        ServicesRepository $servicesRepository,
        RulesRepository $rulesRepository,
        DriverBrowser $driverBrowser
    ) {
        $this->leadsRepository = $leadsRepository;
        $this->providerRepository = $providerRepository;
        $this->accountRepository = $accountRepository;
        $this->servicesRepository = $servicesRepository;
        $this->rulesRepository = $rulesRepository;
        $this->driverBrowser = $driverBrowser;
    }

    public function syncProfiles()
    {
        $driver = $this->driverBrowser->getDriver();
        $this->login($driver);
        $this->getProfiles($driver);
    }

    public function syncServices(Account $account)
    {
        $driver = $this->driverBrowser->getDriver();
        $this->login($driver);
        $this->getServices($account, $driver);
    }

    public function syncLeads(Account $account)
    {
        $driver = $this->driverBrowser->getDriver();

        $this->login($driver);

        $this->switchToAccount($account->account_link, $driver);
        $this->navigateToLeads($driver);

        $this->navigateToInbox($driver);

        $this->getLeads($account, $driver);

        //DriverBrowser::kill();
        $driver->quit();
    }

    public function getLeadAdditionalInformation(Leads $lead)
    {
        $driver = $this->driverBrowser->getDriver();

        /** @var Account $account */
        $account = $this->accountRepository->find($lead->account_id);

        $this->login($driver);

        $this->switchToAccount($account->account_link, $driver);
        $this->navigateToLeads($driver);
        $this->navigateToInbox($driver);

        $lead = $this->getEventInformation($lead, $driver);

        if (!is_null($lead) && $lead->is_responded == 0 && !empty($lead->event_services && !empty($lead->event_location && !empty($lead->event_date)))) {
            AnswerLeads::dispatch($lead)->onQueue('leads_answer');
        }

        //DriverBrowser::kill();
        $driver->quit();
    }

    public function answerLead(Account $account, Leads $lead, Rules $rule, $accept = false)
    {
//        $this->driver = DriverBrowser::getInstance();
        $driver = $this->driverBrowser->getDriver();

        $this->login($driver);

        $this->switchToAccount($account->account_link, $driver);
        $this->navigateToLeads($driver);
        $this->navigateToInbox($driver);

        $lead = $this->fulfillSubmitQuote($driver, $lead, $rule, $accept);

        if ($lead->is_responded) {

            $message = (new LeadAnsweredMail($lead, $rule, $accept))
                        ->onConnection('redis')
                        ->onQueue('mail_lead_answered');

            Mail::to($lead->client_email)->bcc(
                ['sean@artsyevents.com', 'leandro.freire08@gmail.com', 'quotezap@gmail.com']
            )->queue($message);
        }

        //DriverBrowser::kill();
        $driver->quit();
    }

    public function getServices(Account $account, RemoteWebDriver $driver)
    {
        $this->switchToAccount($account->account_link, $driver);
        $this->navigateToCategories($driver);

        $services = $driver->findElements(WebDriverBy::id("chkCategories"));
        $selectedServices = [];

        /** @var RemoteWebElement $service */
        foreach ($services as $service) {
            if ($service->isSelected()) {
                $parentLabel = $service->findElement(WebDriverBy::xpath("./.."));
                $selectedServices[(int) $service->getAttribute('value')] = $parentLabel->getText();
            }
        }

        foreach ($selectedServices as $externalServiceId => $value) {
            $this->servicesRepository->create(
                [
                    'name' => $value,
                    'internal_service_id' => $externalServiceId,
                    'account_id' => $account->id
                ]
            );
        }
    }

    private function fulfillSubmitQuote(RemoteWebDriver $driver, Leads $lead, Rules $rule, $accept = false)
    {
        try {

            $driver->get("https://mcp.thebash.com/Bid/" . $lead->bid_id);

            if (!$accept) {
                $buttonSubmitNoQuote = $driver->findElement(WebDriverBy::xpath('//*[@id="submit-no-bid-button"]'));
                $buttonSubmitNoQuote->click();
                return $lead;
            }

            $radioResponseTypeYes = $driver->findElement(WebDriverBy::xpath('/html/body/div[2]/div[2]/div[2]/h2[1]/label/input'));
            $radioResponseTypeYes->click();

            $formYesBid = $driver->findElement(WebDriverBy::id('bidYesForm'));

            $this->fulfillRatesAndDepositsSection($formYesBid, $rule, $accept);
            //            $this->fulfillBalanceAndPaymentOptionsSection($formYesBid, $rule);
            $this->fulfillAdditionalInfoSection($formYesBid, $rule, $lead, $accept);

            // Submit
            $buttonSubmitYesQuote = $formYesBid->findElement(WebDriverBy::id('submit-yes-bid-button'));
            $buttonSubmitYesQuote->click();

        } catch (\Exception $e) {
            Log::error("Unable to answer the lead: " . $e->getMessage());
            throw new \Exception($e);
        }

        $lead = $this->getClientInformation($lead, $driver);

        return $lead;
    }

    private function fulfillRatesAndDepositsSection(RemoteWebElement $formYesBid, Rules $rule, $accept = false)
    {
        // Rate elements
        $textRate = $formYesBid->findElement(WebDriverBy::id('Rate'));
        $selectRateTimePeriodID = new WebDriverSelect($formYesBid->findElement(WebDriverBy::id('RateTimePeriodID')));

        $textRate->sendKeys(0);
        if ($rule->hourly_rate > 0) {
            if ($accept) {
                $textRate->clear();
                $textRate->sendKeys($rule->hourly_rate);
                $selectRateTimePeriodID->selectByValue(self::VIEW_LEAD_HOURLY_RATE);
            } else {
                $textRate->sendKeys(0);
            }
        }

        // Travel elements

        if ($rule->travel_charge > 0) {
            $checkboxHasOptionalTravelCharges = $formYesBid->findElement(WebDriverBy::id('HasOptionalTravelCharges'));
            $radioExactDollarAmount = $formYesBid->findElement(WebDriverBy::xpath('/html/body/div[2]/div[2]/div[2]/form[1]/ul[1]/li[2]/ul/li[1]/label/input'));
            $textTravelCharge = $formYesBid->findElement(WebDriverBy::id('TravelCharge'));

            if ($accept) {
                $checkboxHasOptionalTravelCharges->click();
                $radioExactDollarAmount->click();
                $textTravelCharge->sendKeys($rule->travel_charge);
            }
        }
    }

    private function fulfillAdditionalInfoSection(RemoteWebElement $formYesBid, Rules $rule, Leads $lead, $accept = false)
    {
        // ADDITIONAL INFO
        $textareaNotes = $formYesBid->findElement(WebDriverBy::id('Notes'));

        if (!empty($rule->text_response) && $accept) {
            $responseText = str_replace("{{client_name}}", $lead->client_name, $rule->text_response);
            $textareaNotes->clear();
            $responseText = str_replace("<p>","\n\n",$responseText);
            $textareaNotes->sendKeys(strip_tags($responseText));
        } else {
            $responseText = str_replace("<p>","\n\n",$rule->text_response_no);
            $textareaNotes->sendKeys(strip_tags($responseText));
        }
    }

    private function getClientInformation(Leads $lead, RemoteWebDriver $driver)
    {
        $clientName = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[2]/dl/dd[1]'))->getText();
        $clientEmail = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[2]/dl/dd[3]/a'))->getText();
        $clientPhone = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[2]/dl/dd[4]'))->getText();

        $lead->client_name = $clientName;
        $lead->client_email = $clientEmail;
        $lead->client_phone = $clientPhone;

        $lead->is_responded = 1;
        $lead->save();

        return $lead;
    }

    private function getEventInformation(Leads $lead, RemoteWebDriver $driver)
    {
        $driver->get("https://mcp.thebash.com/Bid/" . $lead->bid_id);

        /**
         * Skip leads that do not have the respond form
         */
        if (!$this->isElementPresent(WebDriverBy::xpath('/html/body/div[2]/div[2]/div[2]/h2[1]/label/input'), $driver)) {
            $this->leadsRepository->delete($lead->id);
            return null;
        }

        $location = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[1]/dl/dd[4]'))->getText();
        $location = preg_replace('/[^0-9]/','', $location);

        $lead->event_location = (!empty($location)) ? $location : 0;
        $startTime = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[1]/dl/dd[5]'))->getText();
        $lead->event_start_time = explode(" ", $startTime)[0];
        $eventLength = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[1]/dl/dd[6]'))->getText();
        $lead->event_length = preg_replace('/[^0-9]/','', $eventLength);
        $lead->event_guests = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[1]/dl/dd[7]'))->getText();
        $lead->event_services = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[1]/dl/dd[8]'))->getText();

        if ($this->isElementPresent(WebDriverBy::xpath('//*[@id="lead-info"]/div[1]/dl/dd[9]'), $driver)) {
            $lead->event_budget = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[1]/dl/dd[9]'))->getText();
        }

        $clientName = $driver->findElement(WebDriverBy::xpath('//*[@id="lead-info"]/div[2]/dl/dd[1]'))->getText();
        $lead->client_name = trim(explode("(", $clientName)[0]);

        if ($this->isElementPresent(WebDriverBy::xpath('//*[@id="ng-app"]/body/div[2]/div[2]/div[2]/div[4]'), $driver)) {
            $lead = $this->handleAlreadyAnsweredLead($lead);
        }

        $lead->save();

        return $lead;
    }

    /**
     * Handle lead that was already answered as No but is still without answer on lead listing
     *
     * @param Leads $lead
     * @return Leads
     */
    public function handleAlreadyAnsweredLead(Leads $lead)
    {
        $lead->is_responded = 1;
        return $lead;
    }

    private function isElementPresent(WebDriverBy $by, RemoteWebDriver $driver)
    {
        try {
            $driver->findElement($by);
            return true;
        } catch (NoSuchElementException $e) {
            return false;
        }
    }

    public function login(RemoteWebDriver $driver)
    {
        /** @var Provider $provider */
        $provider = $this->providerRepository->find(1)->first();

        try {
            $driver->get($provider->url);
        } catch (WebDriverCurlException $e) {
            Log::error("Unable to open login URL: " . $e->getMessage());
            $driver->get($provider->url);
        }

        $driver->findElement(WebDriverBy::id(self::LOGIN_USERNAME_ELEMENT_ID))
            ->sendKeys($provider->username);
        $driver->findElement(WebDriverBy::id(self::LOGIN_PASSWORD_ELEMENT_ID))
            ->sendKeys(Crypt::decryptString($provider->password))
            ->sendKeys(array(WebDriverKeys::ENTER));

        $this->waitForAjax($driver);
    }

    private function getProfiles(RemoteWebDriver $driver)
    {
        $elements = $driver->findElements(WebDriverBy::cssSelector('#accounts-table tbody tr'));

        for ($i = 0; $i < count($elements); $i++) {
            $accountName = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="accounts-table"]/tbody/tr[' . ($i + 1) . ']/td[1]/ul/li[2]/a'))->getText();
            $accountSwitchLink = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="accounts-table"]/tbody/tr[' . ($i + 1) . ']/td[1]/ul/li[2]/a'))->getAttribute("href");
            $accountId = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="accounts-table"]/tbody/tr[' . ($i + 1) . ']/td[1]/ul/li[3]'))->getText();

            // Check if account already exist
            $accountModel = $this->accountRepository->all(['internal_account_id' => $accountId], null, 1)->first();
            if ($accountModel) {
                continue;
            }

            $accountProfiles[$i] = [
                "account_name" => $accountName,
                "internal_account_id" => $accountId,
                "account_link" => str_replace("javascript:", "", $accountSwitchLink),
                "provider_id" => 1
            ];

            $accountModel = $this->accountRepository->create($accountProfiles[$i]);
            Log::info('Account with ID: ' . $accountModel->internal_account_id . ' synced.');
        }
    }

    private function switchToAccount($accountLink, RemoteWebDriver $driver)
    {
        $driver->executeScript($accountLink);
        $this->waitForAjax($driver);
    }

    private function navigateToLeads(RemoteWebDriver $driver)
    {
        $driver->get("https://mcp.thebash.com/leads.aspx");
    }

    private function navigateToInbox(RemoteWebDriver $driver)
    {
        $driver->get("https://mcp.thebash.com/ArchiveGigRequests.asp?k=&folderID=1&sortcolumn=gigID&sort=DESC");
    }

    private function navigateToCategories(RemoteWebDriver $driver)
    {
        $driver->get("https://mcp.thebash.com/categories.aspx");
    }

    private function getLeads(Account $accountModel, RemoteWebDriver $driver)
    {
        $elements = $driver->findElements(WebDriverBy::cssSelector('#yourleads table tbody tr'));

        for ($i=2; $i <= 12; $i++) {
//        for ($i=2; $i < count($elements); $i++) {

            $response = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="yourleads"]/table/tbody/tr['.$i.']/td[8]'))->getText();

            // Don't sync leads that were already answered
            if ($response === "Y") {
                continue;
            }

            $eventDate = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="yourleads"]/table/tbody/tr['.$i.']/td[3]'))->getText();
            $eventDate = Carbon::parse($eventDate, 'UTC');
            $now = Carbon::now('UTC');
            $now->modify('-1 day');

            // Don't sync past leads
            if ($eventDate->lte($now)) {
                continue;
            }

            $gigId = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="yourleads"]/table/tbody/tr['.$i.']/td[2]'))->getText();

            // Check if lead was already synced
            /** @var Leads $leadModel */
            $leadModel = Leads::query()->where('gig_id', '=', $gigId)->withTrashed()->first();
//            $leadModel = $this->leadsRepository->all(['gig_id' => $gigId])->first();

            /**
             * @TODO Try to reprocess the leads that have is_responded = 0 in order to update to responded
             */

            if ($leadModel) {
                continue;
            }

            $bidId = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="yourleads"]/table/tbody/tr['.$i.']/td[2]/a'))->getAttribute('href');
            $venue = $elements[$i]->findElement(WebDriverBy::xpath('//*[@id="yourleads"]/table/tbody/tr['.$i.']/td[7]'))->getText();


            $lead = [
                "gig_id" => $gigId,
                "bid_id" => preg_replace('/[^0-9]/','', $bidId),
                "event_date" => $eventDate->toDateTimeString(),
                "venue" => $venue,
                "is_responded" => false,
                "account_id" => $accountModel->id
            ];

            try {
                $leadModel = $this->leadsRepository->create($lead);
                SyncLeadsAdditionalInformation::dispatch($leadModel)->onQueue('leads');
                Log::info('Dispatched lead with gig_id: ' . $leadModel->gig_id);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}

