<?php

namespace App\Mail;

use App\Models\Account;
use App\Models\Leads;
use App\Models\Rules;
use App\Repositories\AccountRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LeadAnsweredMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Leads
     */
    protected $lead;

    /**
     * @var Rules
     */
    protected $rule;

    /**
     * @var bool
     */
    protected $answeredYes = false;

    /**
     * LeadAnsweredMail constructor.
     * @param Leads $lead
     * @param Rules $rule
     * @param bool $accept
     */
    public function __construct(
        Leads $lead,
        Rules $rule,
        $accept = false
    ) {
        $this->lead = $lead;
        $this->rule = $rule;
        $this->answeredYes = $accept;
    }

    /**
     * @param AccountRepository $accountRepository
     * @return LeadAnsweredMail
     */
    public function build(AccountRepository $accountRepository)
    {
        /** @var Account $account */
        $account = $accountRepository->find($this->lead->account_id);

        $replyEmail = $account->reply_email;
        return $this->from($replyEmail, $account->account_name)
                    ->subject($this->lead->client_name . " - your Quote for " . $this->lead->event_services)
                    ->view('emails.lead.answered')
                    ->with('rule', $this->rule)
                    ->with('answeredYes', $this->answeredYes)
                    ->with('accountName', $account->account_name);
    }
}
