<?php

namespace App\Http\Controllers;

use App\Charts\DashboardLeadsPerAccountChart;
use App\Charts\DashboardTodayLeadsChart;
use App\Models\Account;
use App\Models\Leads;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalLeadsSyncedChart = $this->getTotalLeadsSynced();
        $todayLeadsChart = $this->getLastWeekLeads();

        return view('home', [
            'totalLeadsChart' => $totalLeadsSyncedChart,
            'todayLeadsChart' => $todayLeadsChart
        ]);
    }

    private function getTotalLeadsSynced()
    {
        $chart = new DashboardLeadsPerAccountChart();
        $accounts = Account::all();

        /** @var Account $account */
        foreach ($accounts as $account) {
            $leadsPerAccount[] = Leads::query()
                ->where('account_id', '=', $account->id)
                ->count();

            $accountNames[] = $account->account_name;

        }
        $chart->labels($accountNames);
        $chart->dataset('Leads Synced Per Account', 'horizontalBar', $leadsPerAccount)->options([
            'backgroundColor' => 'rgba(60, 141, 188, 0.7)',
        ]);

        return $chart;
    }

    private function getLastWeekLeads()
    {
        $chart = new DashboardTodayLeadsChart();

        $dates = collect();
        foreach( range( -6, 0 ) AS $i ) {
            $date = Carbon::now()->addDays( $i )->format( 'Y-m-d' );
            $dates->put( $date, 0);
        }

        $leads = Leads::where( 'created_at', '>=', $dates->keys()->first() )
            ->groupBy( 'date' )
            ->orderBy( 'date' )
            ->get( [
                DB::raw( 'DATE( created_at ) as date' ),
                DB::raw( 'COUNT( * ) as "count"' )
            ] )
            ->pluck( 'count', 'date' );

        $dates = $dates->merge( $leads );

        $weekDay = [];
        $dataset = [];
        foreach ($dates as $date => $count) {
            $dateObj = new Carbon($date);
            $weekDay[] = $dateObj->format('l');
            $dataset[] = $count;
        }

        $chart->labels($weekDay);
        $chart->dataset('Leads synced Last Week', 'line', $dataset)->options([
            'backgroundColor' => 'rgba(60, 141, 188, 0.7)'
        ]);

        return $chart;
    }
}
