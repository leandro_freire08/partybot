<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRulesRequest;
use App\Http\Requests\UpdateRulesRequest;
use App\Models\Rules;
use App\Repositories\RulesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RulesController extends AppBaseController
{
    /** @var  RulesRepository */
    private $rulesRepository;

    public function __construct(RulesRepository $rulesRepo)
    {
        $this->middleware('auth');
        $this->rulesRepository = $rulesRepo;
    }

    /**
     * Display a listing of the Rules.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $rules = $this->rulesRepository->all(['account_id' => $request->get('account_id')])->orderBy('sort_order', 'asc');

        return view('rules.index')
            ->with('rules', $rules);
    }

    public function updateRulesOrder(Request $request)
    {
        $data = $request->all();

        $sortOrder = json_decode($data['sort']);
        $sortOrder = $sortOrder[0];

        foreach ($sortOrder as $key => $value) {
            /** @var Rules $rule */
            $rule = $this->rulesRepository->find($value->id);
            $rule->sort_order = $key;
            $rule->save();
        }

        return response()->json(['success'=>'Rules Order Updated']);
    }

    /**
     * Show the form for creating a new Rules.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $accountId = $request->get('account_id');
        return view('rules.create', ['account_id' => $accountId]);
    }

    /**
     * Store a newly created Rules in storage.
     *
     * @param CreateRulesRequest $request
     *
     * @return Response
     */
    public function store(CreateRulesRequest $request)
    {
        $input = $request->all();

        $rules = $this->rulesRepository->create($input);

        Flash::success('Rules saved successfully.');

        return redirect(route('accounts.show', ['id' => $input['account_id']]));
    }

    /**
     * Display the specified Rules.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rules = $this->rulesRepository->find($id);

        if (empty($rules)) {
            Flash::error('Rules not found');

            return redirect(route('rules.index'));
        }

        return view('rules.show')->with('rules', $rules);
    }

    /**
     * Show the form for editing the specified Rules.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $rules = $this->rulesRepository->find($id);

        if (empty($rules)) {
            Flash::error('Rules not found');

            return view('rules.edit')->with('rules', $rules)->with('account_id', $request->get('account_id'));
        }

        return view('rules.edit')->with('rules', $rules)->with('account_id', $request->get('account_id'));
    }

    /**
     * Update the specified Rules in storage.
     *
     * @param int $id
     * @param UpdateRulesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRulesRequest $request)
    {
        $rules = $this->rulesRepository->find($id);

        if (empty($rules)) {
            Flash::error('Rules not found');

            return redirect(route('accounts.show', [$rules->account_id]));
        }

        $rules = $this->rulesRepository->update($request->all(), $id);

        Flash::success('Rules updated successfully.');

        return redirect(route('accounts.show', [$rules->account_id]));
    }

    /**
     * Remove the specified Rules from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rules = $this->rulesRepository->find($id);

        if (empty($rules)) {
            Flash::error('Rules not found');

            return redirect(route('accounts.show', [$rules->account_id]));
        }

        $this->rulesRepository->delete($id);

        Flash::success('Rules deleted successfully.');

        return redirect(route('accounts.show', [$rules->account_id]));
    }
}
