<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLeadsRequest;
use App\Http\Requests\UpdateLeadsRequest;
use App\Repositories\LeadsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class LeadsController extends AppBaseController
{
    /** @var  LeadsRepository */
    private $leadsRepository;

    public function __construct(LeadsRepository $leadsRepo)
    {
        $this->middleware('auth');
        $this->leadsRepository = $leadsRepo;
    }

    /**
     * Display a listing of the Leads.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $leads = $this->leadsRepository->paginate(15);

        return view('leads.index')
            ->with('leads', $leads);
    }

    /**
     * Show the form for creating a new Leads.
     *
     * @return Response
     */
    public function create()
    {
        return view('leads.create');
    }

    /**
     * Store a newly created Leads in storage.
     *
     * @param CreateLeadsRequest $request
     *
     * @return Response
     */
    public function store(CreateLeadsRequest $request)
    {
        $input = $request->all();

        $leads = $this->leadsRepository->create($input);

        Flash::success('Leads saved successfully.');

        return redirect(route('leads.index'));
    }

    /**
     * Display the specified Leads.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        return view('leads.show')->with('leads', $leads);
    }

    /**
     * Show the form for editing the specified Leads.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        return view('leads.edit')->with('leads', $leads);
    }

    /**
     * Update the specified Leads in storage.
     *
     * @param int $id
     * @param UpdateLeadsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLeadsRequest $request)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        $leads = $this->leadsRepository->update($request->all(), $id);

        Flash::success('Leads updated successfully.');

        return redirect(route('leads.index'));
    }

    /**
     * Remove the specified Leads from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        $this->leadsRepository->delete($id);

        Flash::success('Leads deleted successfully.');

        return redirect(route('leads.index'));
    }
}
