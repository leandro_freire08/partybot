<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Leads;
use Faker\Generator as Faker;

$factory->define(Leads::class, function (Faker $faker) {

    return [
        'gig_id' => $faker->word,
        'event_date' => $faker->word,
        'client_name' => $faker->word,
        'client_email' => $faker->word,
        'client_phone' => $faker->word,
        'venue' => $faker->word,
        'is_responded' => $faker->word,
        'account_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
