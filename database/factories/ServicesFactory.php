<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Services;
use Faker\Generator as Faker;

$factory->define(Services::class, function (Faker $faker) {

    return [
        'account_id' => $faker->randomDigitNotNull,
        'internal_service_id' => $faker->word,
        'name' => $faker->word
    ];
});
