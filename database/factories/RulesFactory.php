<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rules;
use Faker\Generator as Faker;

$factory->define(Rules::class, function (Faker $faker) {

    return [
        'field' => $faker->word,
        'condition' => $faker->word,
        'value' => $faker->word,
        'account_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
