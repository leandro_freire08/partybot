<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeadsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gig_id', 50);
            $table->string('bid_id', 50);
            $table->dateTime('event_date');
            $table->string('event_location',255)->nullable();
            $table->string('event_start_time',255)->nullable();
            $table->string('event_length',255)->nullable();
            $table->string('event_guests',255)->nullable();
            $table->string('event_services',255)->nullable();
            $table->string('event_budget',255)->nullable();
            $table->string('client_name', 50)->nullable();
            $table->string('client_email', 50)->nullable();
            $table->string('client_phone', 50)->nullable();
            $table->string('venue', 255);
            $table->boolean('is_responded');
            $table->string('account_id', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads');
    }
}
