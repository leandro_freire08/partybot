<!-- Account Id Field -->
<div class="form-group">
    {!! Form::label('account_id', 'Account Id:') !!}
    <p>{!! $services->account_id !!}</p>
</div>

<!-- Internal Service Id Field -->
<div class="form-group">
    {!! Form::label('internal_service_id', 'Internal Service Id:') !!}
    <p>{!! $services->internal_service_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $services->name !!}</p>
</div>

