@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            New Rule
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'rules.store', 'id' => 'form_rules']) !!}

                        @include('rules.fields', ['account_id' => $account_id])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
