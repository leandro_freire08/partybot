<!-- Name Field -->
<div class="form-group col-sm-7">
    {!! Form::label('name', 'Rule Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Hourly Rate Field -->
<div class="form-group col-sm-7">
    {!! Form::label('hourly_rate', 'Hourly Rate') !!}
    {!! Form::number('hourly_rate', null, ['class' => 'form-control']) !!}
</div>


<!-- Travel Charge Field -->
<div class="form-group col-sm-7">
    {!! Form::label('travel_charge', 'Added Travel Charge') !!}
    {!! Form::number('travel_charge', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Response Field -->
<div class="form-group col-sm-7">
    {!! Form::label('text_response', 'Text response for accepted lead') !!}
    {!! Form::textarea('text_response', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Response No Field -->
<div class="form-group col-sm-7">
    {!! Form::label('text_response_no', 'Text response for refused lead') !!}
    {!! Form::textarea('text_response_no', null, ['class' => 'form-control']) !!}
</div>

<div>
    {!! Form::hidden('filter', null, ['id' => 'filter_hidden']) !!}
    {!! Form::hidden('account_id', $account_id, ['id' => 'filter_hidden']) !!}
</div>

<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="form-group col-sm-12">

    {!! Form::label('', 'Conditions') !!}

    <div class="flash-message alert-dismissible" role="alert" style="display: none">
        <span></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div id="builder"></div>
            <div class="btn-group">
                <button id="btn-get-sql" type="button" class="btn btn-warning parse-json" data-toggle="modal" data-target="#showSQL">Show Query</button>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="showSQL" tabindex="-1" role="dialog" aria-labelledby="centerTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="centerTitle">Query</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'submit_rule']) !!}
    <a href="{!! route('accounts.show', [$account_id]) !!}" class="btn btn-default">Cancel</a>
</div>


<script type="text/javascript">

    window.onload = function () {

        // Load saved rule
        var saved_rule = <?php echo (isset($rules) && !empty($rules)) ? $rules->filter : 'null' ;?>;

        // Fix for Bootstrap Datepicker
        $('#builder').on('afterUpdateRuleValue.queryBuilder', function(e, rule) {
            if (rule.filter.plugin === 'datepicker') {
                rule.$el.find('.rule-value-container input').datepicker('update');
            }
        });

        $('#builder').queryBuilder({
            plugins: ['bt-tooltip-errors', 'sortable', 'filter-description'],
            filters: <?php echo \App\Models\Rules::getFieldsToFilter(); ?>,
            rules: saved_rule
        });

        $('#submit_rule').on('click', function(e) {
            var result = $('#builder').queryBuilder('getRules');
            $("#filter_hidden").val(JSON.stringify(result));
            $("#form_rules").submit();

        });

        $('#showSQL').on('show.bs.modal', function (e) {
            var result = $('#builder').queryBuilder('getSQL', false, true);

            if (result.sql.length) {
                var modal = $(this);
                modal.find('.modal-body').html("<pre>" + result.sql + "</pre>");
            }
        });

        tinymce.init({selector:'textarea'});
    };

</script>
