@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rule: {{$rules->id}}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($rules, ['route' => ['rules.update', $rules->id], 'method' => 'patch']) !!}

                        @include('rules.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
