<?php
    /** @var $rules \App\Models\Rules */
    /** @var $account \App\Models\Account */
?>

<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="content-header">
    <h1 class="pull-left">Conditions</h1>
</section>
<div class="content">
    <div class="clearfix"></div>

    <div class="flash-message alert-dismissible" role="alert" style="display: none">
        <span></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div id="builder"></div>
            <div class="btn-group">
                <button id="btn-save" class="btn btn-primary parse-json" data-target="plugins">Save</button>
                <button id="btn-get-sql" class="btn btn-warning parse-json" data-toggle="modal" data-target="#showSQL">Show Query</button>

            </div>
        </div>
    </div>

</div>

@if(isset($rules))
    <section class="content-header">
        <h1 class="pull-left">Apply values</h1>
        <h1 class="pull-right">
            @if(isset($rules))
                <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('rules.edit', [$rules->id]) !!}">Edit</a>
            @else
                <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('rules.create') !!}">Add New</a>
            @endif

        </h1>
    </section>
    <div class="clearfix"></div>
    @if(isset($rules))
        @include('rules.show')
    @endif
@endif

<!-- Modal -->
<div class="modal fade" id="showSQL" tabindex="-1" role="dialog" aria-labelledby="centerTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="centerTitle">Query</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    window.onload = function () {

        // Load saved rule
        var saved_rule = <?php echo (!is_null($rules)) ? $rules->filter : 'null' ;?>;

        // Fix for Bootstrap Datepicker
        $('#builder').on('afterUpdateRuleValue.queryBuilder', function(e, rule) {
            if (rule.filter.plugin === 'datepicker') {
                rule.$el.find('.rule-value-container input').datepicker('update');
            }
        });

        $('#builder').queryBuilder({
            plugins: ['bt-tooltip-errors', 'sortable', 'filter-description'],
            filters: <?php echo \App\Models\Rules::getFieldsToFilter(); ?>,
            rules: saved_rule
        });

        $('#btn-save').on('click', function() {
            var result = $('#builder').queryBuilder('getRules');

            if (!$.isEmptyObject(result)) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method:'POST',
                    url:'/admin/rules/ajaxCreate',
                    data: {'filter': result, 'account_id': <?php echo $account->id;?>},
                    success:function(data){
                        $('div.flash-message > span').html(data.success);
                        $('div.flash-message').addClass('alert alert-success');
                        $('div.flash-message').fadeIn();
                        window.location.href = '/admin/accounts/<?php echo $account->id ?>';
                    }
                });
            }
        });

        $('#showSQL').on('show.bs.modal', function () {
            var result = $('#builder').queryBuilder('getSQL', false, true);

            if (result.sql.length) {
                var modal = $(this);
                modal.find('.modal-body').html("<pre>" + result.sql + "</pre>");
            }
        })
    };

</script>

