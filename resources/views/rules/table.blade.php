<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="flash-message alert-dismissible" role="alert" style="display: none">
    <span></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="clearfix"></div>
<div class="table-responsive">
    <table class="table table-hover" id="rules-table">
        <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Sort Order</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($rules as $rule)
            <tr data-id="{{$rule->id}}" data-name="{{$rule->name}}">
                <td><i class="glyphicon glyphicon-sort"></i></td>
                <td>{!! $rule->id !!}</td>
                <td>{!! $rule->name !!}</td>
                <td>{!! $rule->sort_order !!}</td>
                <td>
                    {!! Form::open(['route' => ['rules.destroy', $rule->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('rules.edit', [$rule->id, 'account_id' => $account_id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<style>
    .dragged {
        position: absolute;
        top: 0;
        opacity: 0.5;
        z-index: 2000;
    }

    #rules-table tbody tr {
        cursor: pointer;
    }

    #rules-table tbody tr.placeholder {
        display: block;
        background: red;
        position: relative;
        margin: 0;
        padding: 0;
        border: none;
    }

    #rules-table tbody tr.placeholder:before {
        content: "";
        position: absolute;
        width: 0;
        height: 0;
        border: 5px solid transparent;
        border-left-color: red;
        margin-top: -5px;
        left: -5px;
        border-right: none;
    }

</style>

<script>

    window.onload = function () {

        // Sortable rows
        var group = $('#rules-table').sortable({
            group: 'serialization',
            delay: 500,
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function  ($item, container, _super) {

                var data = group.sortable("serialize").get();

                var newOrder = JSON.stringify(data, null, ' ');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method:'POST',
                    url:'/admin/rules/updateRulesOrder',
                    data: {'sort': newOrder},
                    success:function(data){
                        $('div.flash-message > span').html(data.success);
                        $('div.flash-message').addClass('alert alert-success');
                        $('div.flash-message').fadeIn();
                        window.location.href = '/admin/accounts/<?php echo $account_id ?>';
                    }
                });

                _super($item, container);
            }
            });
    }

</script>
