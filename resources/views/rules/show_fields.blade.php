<!-- Hourly Rate Field -->
<div class="form-group col-sm-7">
    {!! Form::label('hourly_rate', 'Hourly Rate') !!}
    <p>{!! $rules->hourly_rate !!}</p>
</div>


<!-- Travel Charge Field -->
<div class="form-group col-sm-7">
    {!! Form::label('travel_charge', 'Added Travel Charge') !!}
    <p>{!! $rules->travel_charge !!}</p>
</div>

<!-- Text Response Field -->
<div class="form-group col-sm-7">
    {!! Form::label('text_response', 'Canned text response') !!}
    <p>{!! $rules->text_response !!}</p>
</div>

