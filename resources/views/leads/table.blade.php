<div class="table-responsive">
    <table class="table table-hover" id="leads-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Gig Id</th>
                <th>Event Date</th>
                <th>Client Name</th>
                <th>Client Email</th>
                <th>Client Phone</th>
                <th>Venue</th>
                <th>Is Responded</th>
                <th>Account</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php /** @var $lead \App\Models\Leads */ ?>
        @foreach($leads as $lead)
            <tr>
                <td>{!! $lead->id !!}</td>
                <td>{!! $lead->gig_id !!}</td>
                <td>{!! $lead->event_date !!}</td>
                <td>{!! $lead->client_name !!}</td>
                <td>{!! $lead->client_email !!}</td>
                <td>{!! $lead->client_phone !!}</td>
                <td>{!! $lead->venue !!}</td>
                <td>{!! ($lead->is_responded) ? "Yes" : "No" !!}</td>
                <td>{!! \App\Models\Account::getNameById($lead->account_id) !!}</td>
                <td>
                    {!! Form::open(['route' => ['leads.destroy', $lead->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('leads.show', [$lead->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('leads.edit', [$lead->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $leads->links() }}
</div>
