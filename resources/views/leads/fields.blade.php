<!-- Gig Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gig_id', 'Gig Id:') !!}
    {!! Form::text('gig_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_date', 'Event Date:') !!}
    {!! Form::date('event_date', null, ['class' => 'form-control','id'=>'event_date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#event_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Client Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_name', 'Client Name:') !!}
    {!! Form::text('client_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Client Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_email', 'Client Email:') !!}
    {!! Form::email('client_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Client Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_phone', 'Client Phone:') !!}
    {!! Form::text('client_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Venue Field -->
<div class="form-group col-sm-6">
    {!! Form::label('venue', 'Venue:') !!}
    {!! Form::text('venue', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Responded Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_responded', 'Is Responded:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_responded', 0) !!}
        {!! Form::checkbox('is_responded', '1', null) !!}
    </label>
</div>


<!-- Account Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('internal_account_id', 'Account Id:') !!}
    {!! Form::text('internal_account_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('leads.index') !!}" class="btn btn-default">Cancel</a>
</div>
