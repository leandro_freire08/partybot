<!-- Gig Id Field -->
<div class="form-group">
    {!! Form::label('gig_id', 'Gig Id:') !!}
    <p>{!! $leads->gig_id !!}</p>
</div>

<!-- Event Date Field -->
<div class="form-group">
    {!! Form::label('event_date', 'Event Date:') !!}
    <p>{!! $leads->event_date !!}</p>
</div>

<!-- Event Start Time Field -->
<div class="form-group">
    {!! Form::label('event_date', 'Event Start Time:') !!}
    <p>{!! $leads->event_start_time !!}</p>
</div>

<!-- Event Location Field -->
<div class="form-group">
    {!! Form::label('event_date', 'Event Location:') !!}
    <p>{!! $leads->event_location !!}</p>
</div>

<!-- Event Duration Field -->
<div class="form-group">
    {!! Form::label('event_date', 'Event Duration:') !!}
    <p>{!! $leads->event_length !!}</p>
</div>

<!-- Event Guests Field -->
<div class="form-group">
    {!! Form::label('event_date', 'Event Guests:') !!}
    <p>{!! $leads->event_guests !!}</p>
</div>

<!-- Event Services Field -->
<div class="form-group">
    {!! Form::label('event_date', 'Event Services:') !!}
    <p>{!! $leads->event_services !!}</p>
</div>

<!-- Client Name Field -->
<div class="form-group">
    {!! Form::label('client_name', 'Client Name:') !!}
    <p>{!! $leads->client_name !!}</p>
</div>

<!-- Client Email Field -->
<div class="form-group">
    {!! Form::label('client_email', 'Client Email:') !!}
    <p>{!! $leads->client_email !!}</p>
</div>

<!-- Client Phone Field -->
<div class="form-group">
    {!! Form::label('client_phone', 'Client Phone:') !!}
    <p>{!! $leads->client_phone !!}</p>
</div>

<!-- Venue Field -->
<div class="form-group">
    {!! Form::label('venue', 'Venue:') !!}
    <p>{!! $leads->venue !!}</p>
</div>

<!-- Is Responded Field -->
<div class="form-group">
    {!! Form::label('is_responded', 'Is Responded:') !!}
    <p>{!! $leads->is_responded !!}</p>
</div>

<!-- Account Id Field -->
<div class="form-group">
    {!! Form::label('internal_account_id', 'Account Id:') !!}
    <p>{!! $leads->internal_account_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $leads->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $leads->updated_at !!}</p>
</div>

