<div class="table-responsive">
    <table class="table table-hover" id="providers-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Url</th>
                <th>Username</th>
                <th>Password</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($providers as $provider)
            <tr>
                <td>{!! $provider->name !!}</td>
            <td>{!! $provider->url !!}</td>
            <td>{!! $provider->username !!}</td>
            <td>{!! \Illuminate\Support\Str::limit($provider->password, 30, " (...)") !!}</td>
                <td>
                    {!! Form::open(['route' => ['providers.destroy', $provider->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('providers.show', [$provider->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('providers.edit', [$provider->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
