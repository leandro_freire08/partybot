<li class="{{ Request::is('admin') ? 'active' : '' }}">
    <a href="{{url('/')}}"><i class="fa fa-bar-chart"></i><span>Dashboard</span></a>
</li>

<li class="{{ Request::is('admin/leads*') ? 'active' : '' }}">
    <a href="{!! route('leads.index') !!}"><i class="fa fa-edit"></i><span>Leads</span></a>
</li>

<li class="{{ Request::is('admin/accounts*') ? 'active' : '' }}">
    <a href="{!! route('accounts.index') !!}"><i class="fa fa-user"></i><span>Accounts</span></a>
</li>

<li class="{{ Request::is('admin/providers*') ? 'active' : '' }}">
    <a href="{!! route('providers.index') !!}"><i class="fa fa-globe"></i><span>Providers</span></a>
</li>

<li>
    <a href="/horizon" target="_blank"><i class="fa fa-external-link"></i><span>Queue</span></a>
</li>

<li>
    <a href="<?php echo "http://" . Request::server('SERVER_ADDR') . ":4444/grid/console" ?>" target="_blank"><i class="fa fa-external-link"></i><span>Selenium</span></a>
</li>
