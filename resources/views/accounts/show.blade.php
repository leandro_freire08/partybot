@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Account
        </h1>
        <a href="{!! route('accounts.index') !!}" class="btn btn-default">Back</a>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('accounts.show_fields')
                </div>
            </div>
        </div>
    </div>

    <section class="content-header">
        <h1>Rules</h1>
        <h1>
            <a class="btn btn-primary pull-right" style="margin-top: -25px;" href="{!! route('rules.create', ['account_id' => $account->id]) !!}">Add New</a>
        </h1>
    </section>

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('rules.table', ['account_id' => $account->id])
                </div>
            </div>
        </div>
    </div>
@endsection
