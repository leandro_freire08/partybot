<!-- Account Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('internal_account_id', 'Account Id:') !!}
    {!! Form::text('internal_account_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Account Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_name', 'Account Name:') !!}
    {!! Form::text('account_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Account Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_link', 'Account Link:') !!}
    {!! Form::text('account_link', null, ['class' => 'form-control']) !!}
</div>

<!-- Reply Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reply_email', 'Reply Email:') !!}
    {!! Form::email('reply_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Provider Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('provider_id', 'Provider Id:') !!}
    {!! Form::text('provider_id', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('accounts.index') !!}" class="btn btn-default">Cancel</a>
</div>
