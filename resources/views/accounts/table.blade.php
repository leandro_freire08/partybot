<div class="table-responsive">
    <table class="table table-hover" id="accounts-table">
        <thead>
            <tr>
                <th>Account Id</th>
                <th>Account Name</th>
                <th>Account Link</th>
                <th>Reply Email</th>
                <th>Provider</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($accounts as $account)
            <tr>
                <td>{!! $account->internal_account_id !!}</td>
                <td>{!! $account->account_name !!}</td>
                <td>{!! $account->account_link !!}</td>
                <td>{!! $account->reply_email !!}</td>
                <td>{!! \App\Models\Provider::getNameById($account->provider_id) !!}</td>
                <td>
                    {!! Form::open(['route' => ['accounts.destroy', $account->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('accounts.show', [$account->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('accounts.edit', [$account->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
