<!-- Account Id Field -->
<div class="form-group">
    {!! Form::label('internal_account_id', 'Account Id:') !!}
    <p>{!! $account->internal_account_id !!}</p>
</div>

<!-- Account Name Field -->
<div class="form-group">
    {!! Form::label('account_name', 'Account Name:') !!}
    <p>{!! $account->account_name !!}</p>
</div>

<!-- Account Link Field -->
<div class="form-group">
    {!! Form::label('account_link', 'Account Link:') !!}
    <p>{!! $account->account_link !!}</p>
</div>

<!-- Reply Email Field -->
<div class="form-group">
    {!! Form::label('reply_email', 'Reply Email:') !!}
    <p>{!! $account->reply_email !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $account->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $account->updated_at !!}</p>
</div>

<!-- Provider Id Field -->
<div class="form-group">
    {!! Form::label('provider_id', 'Provider:') !!}
    <p>{!! \App\Models\Provider::getNameById($account->provider_id) !!}</p>
</div>

