@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        {!! $totalLeadsChart->container() !!}
    </div>

    <div class="row">
        {!! $todayLeadsChart->container() !!}
    </div>
</div>
{!! $totalLeadsChart->script() !!}
{!! $todayLeadsChart->script() !!}
@endsection
