### Dependencies

`sudo apt-get install openjdk-8-jre-headless`

`sudo apt-get install supervisor`


### Configuration

The following command will install the latest version of selenium-standalone-server and chromedriver

`sudo ./drivers/install.sh`


Copy the supervisor conf files to:

`/etc/supervisor/conf.d`

Run the following command to update supervisor conf files:

`sudo supervisorctl reread`

Starting dependencies:

`sudo supervisorctl start <program_name>`
