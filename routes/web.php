<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Auth::routes();

Route::get('/admin', 'HomeController@index');

Route::resource('admin/leads', 'LeadsController');

Route::resource('admin/accounts', 'AccountController');

Route::resource('admin/providers', 'ProviderController');

Route::resource('admin/rules', 'RulesController');

Route::post('admin/rules/updateRulesOrder', 'RulesController@updateRulesOrder');


Route::resource('services', 'ServicesController');


//Route::get('mailable', function () {
//    $lead = App\Models\Leads::all()->first();
//    $rule = App\Models\Rules::all()->first();
//
//    return new App\Mail\LeadAnsweredMail($lead, $rule, true);
//});
